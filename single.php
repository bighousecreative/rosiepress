<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 get_header();

 	while ( have_posts() ) : the_post();

 	endwhile;

	get_template_part('includes/structured-data/single');

 get_footer();
