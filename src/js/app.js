// This is the main app file for JS.
// You don't really need to mess with this unless you're adding addition modules from npm
// All code for functions should be created and executed in main.js

import $ from 'jquery';

// Foundation JS relies on a global varaible. In ES6, all imports are hoisted
// to the top of the file so if we used`import` to import Foundation,
// it would execute earlier than we have assigned the global variable.
// This is why we have to use CommonJS require() here since it doesn't
// have the hoisting behavior.
window.jQuery = $;
// require('foundation-sites');

// // Slick Carousel
// require('../../../node_modules/slick-carousel/slick/slick.js');
//
// // What Input
import 'what-input';
//
// // Featherlight
// require('../../../node_modules/featherlight/src/featherlight.js');
//
// // Featherlight Gallery
// require('../../../node_modules/featherlight/src/featherlight.gallery.js');
//
// // Motion UI
// require('../../../node_modules/motion-ui/motion-ui.js');

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './foundation-explicit-pieces';

// Menu 2
// import './menu/menu_2.js';

// Initial Foundation
$(document).foundation();

$(document).ready(function(){

    require('./main.js');

});
