const mix = require('laravel-mix');

mix.sass('src/scss/app.scss', 'dist/css/app.css', {
        includePaths: [
            "./node_modules/foundation-sites/scss",
            "./node_modules/@fortawesome/fontawesome-pro/scss"
        ],
        implementation: require('node-sass')
    })
    .options({
        processCssUrls: false,
        extractVueStyles: false
    })
    .js('src/js/app.js', 'dist/js/app.js')
    // .copy(['one.txt', 'two.txt'], 'output/path')
    // .copy('node_modules/@fortawesome/fontawesome-pro/webfonts/*', 'dist/fonts')

    // .sourceMaps()
    // .webpackConfig({
    //     devtool: 'source-map',
    //     resolve: {
    //         modules: [
    //             path.resolve(__dirname, 'node_modules')
    //         ]
    //     }
    // })
    .browserSync({
        proxy: 'https://wordpress.site',
        files: [
            'dist/css/app.css',  // Generated .css file
            'dist/js/app.js',    // Generated .js file
            '*.+(html|php)',     // Generic .html and/or .php files
        ]
    });
