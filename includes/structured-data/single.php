<script type="application/ld+json">
	<?php
	$jsonData = [
		'@content' => 'http://schema.org',
		'@type' => 'Article',
		'headline' => get_the_title(),
		'description' =>  get_the_excerpt(),

		'url' => get_permalink(),
		'datePublished' => get_the_date('c')
	];
	if(has_post_thumbnail()) {
		$jsonData['image'] = get_the_post_thumbnail_url();
	}
	echo json_encode($jsonData);
	?>
</script>
