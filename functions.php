<?php
/**
 * Author: Ole Fredrik Lie
 * URL: http://olefredrik.com
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

 if (!defined('WP_ENV')) {
     // Fallback if WP_ENV isn't defined in your WordPress config
     // Used to check for 'development' or 'production'
     define('WP_ENV', 'production');
 }

/** Add ACF */
require_once( 'library/acf-init.php' );

/** Add ACF Options Page */
require_once( 'library/acf-options.php' );

/** Mailgun Settings */
require_once( 'library/mailgun.php' );

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Register custom menus */
require_once( 'library/menu.php' );

/** Add menu walkers for top-bar and off-canvas */
require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
require_once( 'library/sticky-posts.php' );

/** Configure responsive image sizes */
require_once( 'library/responsive-images.php' );

/** Add Custom AW Pagination */
require_once( 'library/pagination.php' );

/** Add WordPress Menus */
require_once( 'library/pagination.php' );

/** Remove WP JSON in Header */
require_once( 'library/remove-json.php' );

/** Add Custom Menu */
require_once( 'library/menu.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );
