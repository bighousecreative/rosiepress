<?php
/**
* Enqueue all styles and scripts
*
* Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
* Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
*
* @package FoundationPress
* @since FoundationPress 1.0.0
*/

if ( ! function_exists( 'foundationpress_scripts' ) ) {
    function foundationpress_scripts() {

        // Deregister the jquery version bundled with WordPress.
        wp_deregister_script( 'jquery' );

        // CDN hosted jQuery placed in the header, as some plugins require that jQuery is loaded in the header.
        // wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/javascript/jquery.min.js', array(), '2.1.0', true );

        // If you'd like to cherry-pick the foundation components you need in your project, head over to gulpfile.js and see lines 35-54.
        // It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
        $script = get_template_directory().'/dist/js/app.js';
        $script_mod_time = filemtime($script);

        $style = get_template_directory().'/dist/css/app.css';
        $style_mod_time = filemtime($style);

        wp_enqueue_script( 'rosiepress_js', get_stylesheet_directory_uri() . '/dist/js/app.js', array(), $script_mod_time, true );
        wp_enqueue_style( 'rosiepress_css', get_stylesheet_directory_uri() . '/dist/css/app.css', array(), $style_mod_time, 'all' );

        // Add the comment-reply library on pages where it is necessary
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

    }

    add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
}
