<?php

################################################
#
#  Integrates ACF into Theme
#
################################################

// 3. Hide ACF field group menu item
if(WP_ENV != 'development') add_filter('acf/settings/show_admin', '__return_false');
