<?php

################################################
#
#  Menus
#
################################################

function add_custom_nav() {

    register_nav_menus(
        array(
            'main_navigation_menu' => 'Main Menu',
        )
    );

}

add_action( 'init', 'add_custom_nav' );
