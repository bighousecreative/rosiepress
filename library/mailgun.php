<?php

$swpsmtp_options = array(
	'from_email_field' 			=> 'webmailer@aw-stage.co.uk',
	'from_name_field'   		=> get_bloginfo( 'name' ) . ' Website',
	'smtp_settings'     		=> array(
		'host'               	=> 'smtp.mailgun.org',
		'type_encryption'			=> 'tls',
		'port'              	=> 587,
		'autentication'				=> 'yes',
		'username'						=> 'webmailer@aw-stage.co.uk',
		'password'          	=> 'xeG-fd8-Zeu-zSW'
	)
);

/**
 * Function to add smtp options in the phpmailer_init
 * @return void
 */
if ( ! function_exists ( 'swpsmtp_init_smtp' ) ) {
	function swpsmtp_init_smtp( $phpmailer ) {
		global $swpsmtp_options;
		/* Set the mailer type as per config above, this overrides the already called isMail method */
		$phpmailer->IsSMTP();
		$from_email = $swpsmtp_options['from_email_field'];
    $phpmailer->From = $from_email;
    $from_name  = $swpsmtp_options['from_name_field'];
    $phpmailer->FromName = $from_name;
    $phpmailer->SetFrom($phpmailer->From, $phpmailer->FromName);
		/* Set the SMTPSecure value */
		if ( $swpsmtp_options['smtp_settings']['type_encryption'] !== 'none' ) {
			$phpmailer->SMTPSecure = $swpsmtp_options['smtp_settings']['type_encryption'];
		}

		/* Set the other options */
		$phpmailer->Host = $swpsmtp_options['smtp_settings']['host'];
		$phpmailer->Port = $swpsmtp_options['smtp_settings']['port'];

		/* Always smtp auth with Mailgun, set the username & password */
		$phpmailer->SMTPAuth = true;
		$phpmailer->Username = $swpsmtp_options['smtp_settings']['username'];
		$phpmailer->Password = swpsmtp_get_password();

    //PHPMailer 5.2.10 introduced this option. However, this might cause issues if the server is advertising TLS with an invalid certificate.
    $phpmailer->SMTPAutoTLS = false;
	}
}

if ( ! function_exists( 'swpsmtp_get_password' ) ) {
	function swpsmtp_get_password() {
		global $swpsmtp_options;

    $temp_password = $swpsmtp_options['smtp_settings']['password'];
    $password = "";
    $decoded_pass = base64_decode($temp_password);
    /* no additional checks for servers that aren't configured with mbstring enabled */
    if ( ! function_exists( 'mb_detect_encoding' ) ){
        return $decoded_pass;
    }
    /* end of mbstring check */
    if (base64_encode($decoded_pass) === $temp_password) {  //it might be encoded
        if(false === mb_detect_encoding($decoded_pass)){  //could not find character encoding.
            $password = $temp_password;
        }
        else{
            $password = base64_decode($temp_password);
        }
    }
    else{ //not encoded
        $password = $temp_password;
    }
    return $password;
	}
}

add_action( 'phpmailer_init','swpsmtp_init_smtp');
